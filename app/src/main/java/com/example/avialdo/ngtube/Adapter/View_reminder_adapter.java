package com.example.avialdo.ngtube.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.avialdo.ngtube.R;


/**
 * Created by MUHAMMAD on 6/11/2017.
 */
public class View_reminder_adapter extends ArrayAdapter {
    public View_reminder_adapter(Context context, int resource) {
        super(context, resource);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = LayoutInflater.from(getContext()).inflate(R.layout.reminder_adapter, parent ,false);

        return v;
    }
}
