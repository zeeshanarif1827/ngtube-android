package com.example.avialdo.ngtube.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.avialdo.ngtube.MySingleton;
import com.example.avialdo.ngtube.R;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity {

    TextInputLayout textInputLayout, layoutpass;
    EditText email, pass;
    Button login;
    String et_email, et_pass;
    LoginButton fb;
    ProgressDialog dialog;
    AlertDialog.Builder builder;
    String base_url = "https://wwwubit.000webhostapp.com/NgServices/doctor_login.php";
    CallbackManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        builder = new AlertDialog.Builder(this);
        dialog = new ProgressDialog(this);

        FacebookSdk.sdkInitialize(getApplicationContext());

        textInputLayout = (TextInputLayout) findViewById(R.id.layout_Email);
        layoutpass = (TextInputLayout) findViewById(R.id.layout_pass);
        email = (EditText) findViewById(R.id.login_email);
        pass = (EditText) findViewById(R.id.login_pass);

        login = (Button) findViewById(R.id.login);

        fb = (LoginButton) findViewById(R.id.fb_login_btn);
        manager = CallbackManager.Factory.create();

        fb.registerCallback(manager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                Toast.makeText(Login.this, "Login Successfully", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancel() {
                Toast.makeText(Login.this, "Login Cancelled", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {

            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.setMessage("Signing In...");
                dialog.show();

                et_email = email.getText().toString();
                et_pass = pass.getText().toString();

                if (et_email.equals("") || et_pass.equals("")) {
                    dialog.dismiss();
                    builder.setTitle("Something Went Wrong");
                    displayAlert("Enter a valid username and password");
                } else {
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, base_url, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONArray jsonArray = new JSONArray(response);
                                JSONObject jsonObject = jsonArray.getJSONObject(0);
                                String code = jsonObject.getString("code");

                                if (code.equals("login failed")) {
                                    dialog.dismiss();
                                    builder.setTitle("Login Error");
                                    displayAlert(jsonObject.getString("message"));
                                } else {

                                    dialog.dismiss();

                                    email.setText("");
                                    pass.setText("");

                                    Intent intent = new Intent(Login.this, Home.class);
                                    Bundle bundle = new Bundle();

                                    bundle.putString("doc_id", jsonObject.getString("id"));
                                    bundle.putString("doc_name", jsonObject.getString("name"));
                                    bundle.putString("doc_email", jsonObject.getString("email"));
                                    bundle.putString("doc_city", jsonObject.getString("city"));
                                    bundle.putString("doc_cell", jsonObject.getString("cell"));
                                    bundle.putString("doc_gender", jsonObject.getString("gender"));
                                    bundle.putString("doc_speciality", jsonObject.getString("speciality"));
                                    intent.putExtras(bundle);

                                    startActivity(intent);

                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(Login.this, "Error", Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                            Log.d("Error", error.toString());
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("doc_email", et_email);
                            params.put("doc_password", et_pass);

                            return params;
                        }
                    };

                    MySingleton.getInstance(Login.this).addToRequestQueue(stringRequest);
                }
            }
        });

    }

    private void displayAlert(String message) {

        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                email.setText("");
                pass.setText("");
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        manager.onActivityResult(requestCode, resultCode, data);
    }
}
