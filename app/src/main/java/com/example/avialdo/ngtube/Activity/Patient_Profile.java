package com.example.avialdo.ngtube.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.avialdo.ngtube.R;


public class Patient_Profile extends AppCompatActivity {

    TextView check1,check2,check3,check4,check5,check6,check7,check8,check9;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient__profile);

        check1 = (TextView)findViewById(R.id.profile_name);
        check2 = (TextView)findViewById(R.id.profile_id);
        check3 = (TextView)findViewById(R.id.profile_care);
        check4 = (TextView)findViewById(R.id.profile_cell);
        check5 = (TextView)findViewById(R.id.profile_dob);
        check6 = (TextView)findViewById(R.id.profile_phy_name);
        check7 = (TextView)findViewById(R.id.profile_phy_contact);
        check8 = (TextView)findViewById(R.id.profile_gender);

        Intent intent = getIntent();
        Bundle bd = intent.getExtras();
        if(bd != null)
        {
            String get_pat_id = (String) bd.get("pat_id");
          String get_pat_name = (String) bd.get("patient_name");
            String get_pat_care = (String) bd.get("caregiver");
            String get_pat_dob = (String) bd.get("dob");
            String get_pat_cell = (String) bd.get("contact_no");
            String get_pat_gender = (String) bd.get("gender");
            String get_pat_phyname = (String) bd.get("phy_name");
            String get_pat_phycell = (String) bd.get("phy_contact");



            check1.setText(get_pat_name);
            check2.setText(get_pat_id);
            check3.setText(get_pat_care);
            check4.setText(get_pat_cell);
            check5.setText(get_pat_dob);
            check6.setText(get_pat_phyname);
            check7.setText(get_pat_phycell);
            check8.setText(get_pat_gender);

        }

    }
}
