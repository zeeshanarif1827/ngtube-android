package com.example.avialdo.ngtube.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.avialdo.ngtube.MySingleton;
import com.example.avialdo.ngtube.R;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class SignUp extends AppCompatActivity {

    EditText name,cell,email,pass,confirm;
    MaterialBetterSpinner layout_specialist ,layout_city , layout_gender;
  //  DatabaseHelper databaseHelper;
    Button SignUp;
    CircleImageView doc_dp;
    AlertDialog.Builder builder;
    String base_url= "https://wwwubit.000webhostapp.com/NgServices/doctor_register.php";
    String doc_name, doc_city, doc_cell, doc_email, doc_pass, doc_conf_pass,doc_gender, doc_speciality;

    private static final int GALLERY_INTENT = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        builder = new AlertDialog.Builder(this);

//        databaseHelper = new DatabaseHelper(this);
//        databaseHelper.queryData("CREATE TABLE IF NOT EXISTS doctor_info (ID INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, mobile TEXT , email TEXT, pass TEXT, confirm TEXT, image BLOB, city TEXT , speciality TEXT)");

        final String[] SPINNERLIST = {"select", "Cardiology_Surgeon", "Dermotology_Surgeon", "Orthopedic_Surgeon", "Neuro_Surgeon" , "Other"};
        final String[] CITY = {"select", "Karachi","Lahore","Multan","Islamabad"};
        final String[] GENDER = {"select", "Male","FeMale"};

        name = (EditText)findViewById(R.id.signup_name);
        cell = (EditText)findViewById(R.id.signup_cell);
        email = (EditText)findViewById(R.id.signup_email);
        pass = (EditText)findViewById(R.id.signup_pass);
        confirm = (EditText)findViewById(R.id.signup_conf);
        SignUp = (Button)findViewById(R.id.signup);

        layout_specialist = (MaterialBetterSpinner)findViewById(R.id.specialist_layout);
        layout_city = (MaterialBetterSpinner)findViewById(R.id.city_layout);
        layout_gender = (MaterialBetterSpinner)findViewById(R.id.gender_layout);



        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line, SPINNERLIST);
        ArrayAdapter<String> cityAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line, CITY);
        ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line, GENDER);

        layout_specialist.setAdapter(arrayAdapter);
        layout_city.setAdapter(cityAdapter);
        layout_gender.setAdapter(genderAdapter);


//        upload_doc_dp.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(Intent.ACTION_PICK);
//                intent.setType("image/*");
//                startActivityForResult(intent, GALLERY_INTENT);
//            }
//        });

        SignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                doc_name = name.getText().toString();
                doc_cell= cell.getText().toString();
                doc_email = email.getText().toString();
                doc_pass = pass.getText().toString();
                doc_conf_pass = confirm.getText().toString();
                doc_speciality = layout_specialist.getText().toString();
                doc_city = layout_city.getText().toString();
                doc_gender = layout_gender.getText().toString();


                if (doc_name.equals("") || doc_email.equals("") || doc_pass.equals("") || doc_conf_pass.equals("")
                        || doc_speciality.equals("") || doc_city.equals("") || doc_cell.equals("")) {

                    builder.setTitle("Something Went Wrong");
                    builder.setMessage("Please fill all the fields");
                    displayAlert("input error");
                }

                if(!doc_pass.equals(doc_conf_pass)){
                    builder.setTitle("Something Went Wrong");
                    builder.setMessage("Password and Confirm Password Does not Match");
                    displayAlert("input error");
                }
                else{
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, base_url, new Response.Listener<String>() {

                        public void onResponse(String response) {
                            try {

                                JSONArray jsonArray = new JSONArray(response);
                                Log.e("Error", response.toString());
                                JSONObject jsonObject = jsonArray.getJSONObject(0);
                                String code = jsonObject.getString("code");
                                String message = jsonObject.getString("message");

                                builder.setTitle("Server Response");
                                builder.setMessage(message);
                                displayAlert(code);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            error.printStackTrace();
                            Log.d("Stack", error.toString());
                        }

                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("doc_name", doc_name);
                            params.put("doc_email", doc_email);
                            params.put("doc_password", doc_pass);
                            params.put("doc_city", doc_city);
                            params.put("doc_cell", doc_cell);
                            params.put("doc_gender", doc_gender);
                            params.put("doc_speciality", doc_speciality);

                            return params;
                        }
                    };

                    MySingleton.getInstance(SignUp.this).addToRequestQueue(stringRequest);
                }
            }
        });

    }

    private void displayAlert(final String code) {

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if (code.equals("input error")) {

                    name.setText("");
                    email.setText("");
                    pass.setText("");
                    confirm.setText("");
                    layout_city.setText("");
                    layout_gender.setText("");
                    layout_specialist.setText("");

                } else if (code.equals("registration_Success")) {
                    finish();

                } else if (code.equals("registration_failed")) {

                    name.setText("");
                    email.setText("");
                    pass.setText("");
                    confirm.setText("");
                    layout_city.setText("");
                    layout_gender.setText("");
                    layout_specialist.setText("");

                }
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//
//        Uri uri = data.getData();
//
//        if(requestCode == GALLERY_INTENT && resultCode == RESULT_OK && data!=null){
//
//            try{
//                InputStream inputStream = getContentResolver().openInputStream(uri);
//                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
//                doc_dp.setImageBitmap(bitmap);
//
//            }catch(FileNotFoundException e){
//                e.printStackTrace();
//            }
//        }
//    }

//    private byte[] imageViewToByte(CircleImageView doc_dp) {
//
//        Bitmap bitmap = ((BitmapDrawable)doc_dp.getDrawable()).getBitmap();
//        ByteArrayOutputStream stream = new ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.PNG,100,stream);
//        byte[] bytearray = stream.toByteArray();
//
//        return bytearray;
//    }
}