package com.example.avialdo.ngtube.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.avialdo.ngtube.Activity.Food_Supplement;
import com.example.avialdo.ngtube.Activity.Formula;
import com.example.avialdo.ngtube.Activity.Intolerance;
import com.example.avialdo.ngtube.R;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

/**
 * A simple {@link Fragment} subclass.
 */
public class Diary extends Fragment implements Animation.AnimationListener{

    Button show,formula , show_tolerance,tol, food_supplement;
    Animation animation;
    RelativeLayout hide1,hide2,hide4;
    TextView show_on_click1 , show_on_click2;
    MaterialBetterSpinner pat_name;

    public Diary() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_diary, container, false);

        animation = AnimationUtils.loadAnimation(getContext().getApplicationContext(), R.anim.slide_down);

        show = (Button)v.findViewById(R.id.show);
        show_tolerance = (Button)v.findViewById(R.id.show_tolerance);
        formula = (Button)v.findViewById(R.id.formula);
        tol = (Button)v.findViewById(R.id.tol);
        food_supplement = (Button)v.findViewById(R.id.food_supplement);

        hide1 = (RelativeLayout)v.findViewById(R.id.hide1);
        hide2 = (RelativeLayout)v.findViewById(R.id.hide2);
        hide4 = (RelativeLayout)v.findViewById(R.id.hide4);

        animation.setAnimationListener(this);

        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                hide1.setVisibility(View.VISIBLE);
                hide1.startAnimation(animation);
                hide2.setVisibility(View.VISIBLE);
                hide2.startAnimation(animation);
            }
        });

        show_tolerance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hide4.setVisibility(View.VISIBLE);
                hide4.startAnimation(animation);

            }
        });

        formula.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext() ,Formula.class));
            }
        });

        tol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), Intolerance.class));
            }
        });

        food_supplement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), Food_Supplement.class));
            }
        });

        return v;
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
