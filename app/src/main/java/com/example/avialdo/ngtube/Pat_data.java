package com.example.avialdo.ngtube;

/**
 * Created by MUHAMMAD on 6/13/2017.
 */
public class Pat_data{

    String doc_id, pat_id, pat_name,caregiver,dob,contact_no,gender,phy_name,phy_contact ;

    public Pat_data(String doc_id, String pat_id, String pat_name, String caregiver, String dob, String contact_no, String gender, String phy_name, String phy_contact) {
        this.doc_id = doc_id;
        this.pat_id = pat_id;
        this.pat_name = pat_name;
        this.caregiver = caregiver;
        this.dob = dob;
        this.contact_no = contact_no;
        this.gender = gender;
        this.phy_name = phy_name;
        this.phy_contact = phy_contact;
    }

    public Pat_data() {
    }

    public String getDoc_id() {
        return doc_id;
    }

    public void setDoc_id(String doc_id) {
        this.doc_id = doc_id;
    }

    public String getPat_id() {
        return pat_id;
    }

    public void setPat_id(String pat_id) {
        this.pat_id = pat_id;
    }

    public String getPat_name() {
        return pat_name;
    }

    public void setPat_name(String pat_name) {
        this.pat_name = pat_name;
    }

    public String getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(String caregiver) {
        this.caregiver = caregiver;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getContact_no() {
        return contact_no;
    }

    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhy_name() {
        return phy_name;
    }

    public void setPhy_name(String phy_name) {
        this.phy_name = phy_name;
    }

    public String getPhy_contact() {
        return phy_contact;
    }

    public void setPhy_contact(String phy_contact) {
        this.phy_contact = phy_contact;
    }
}
