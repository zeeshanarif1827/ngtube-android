package com.example.avialdo.ngtube.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.avialdo.ngtube.R;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyProfile extends Fragment {

    public TextView doc_name,doc_city,doc_cell,doc_email,doc_gender,doc_spec;
    CircleImageView circleImageView;
    Button button;
    FragmentTransaction fragmentTransaction;

    public MyProfile() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_my_profile, container, false);

        doc_name = (TextView)v.findViewById(R.id.doc_name);
        doc_city = (TextView)v.findViewById(R.id.doc_city);
        doc_cell = (TextView)v.findViewById(R.id.doc_cell);
        doc_email = (TextView)v.findViewById(R.id.doc_email);
        doc_spec = (TextView)v.findViewById(R.id.doc_spec);
        doc_gender = (TextView)v.findViewById(R.id.doc_gender);

        circleImageView = (CircleImageView)v.findViewById(R.id.dp);

        button = (Button)v.findViewById(R.id.update_doctor_profile);

        Bundle bundle = getActivity().getIntent().getExtras();

            doc_name.setText(bundle.getString("doc_name"));
            doc_email.setText(bundle.getString("doc_email"));
            doc_cell.setText(bundle.getString("doc_cell"));
            doc_city.setText(bundle.getString("doc_city"));
            doc_gender.setText(bundle.getString("doc_gender"));
            doc_spec.setText(bundle.getString("doc_speciality"));


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.content_home, new Update_Doc_Data());
                fragmentTransaction.commit();
            }
        });

//            byte[] testimage = image;
//            Bitmap bitmap = BitmapFactory.decodeByteArray(testimage, 0, testimage.length);

     //       circleImageView.setImageBitmap(bitmap);


            return v;
    }
}
