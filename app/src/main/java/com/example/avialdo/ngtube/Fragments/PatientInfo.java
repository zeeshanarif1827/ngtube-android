package com.example.avialdo.ngtube.Fragments;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.avialdo.ngtube.Activity.Patient_Profile;
import com.example.avialdo.ngtube.MySingleton;
import com.example.avialdo.ngtube.Pat_data;
import com.example.avialdo.ngtube.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class PatientInfo extends Fragment {

    ListView listView;
    ArrayList<Pat_data> list;
    ArrayList<String> list1;
    ProgressDialog dialog;
    AlertDialog.Builder builder;
    String base_url = "https://wwwubit.000webhostapp.com/NgServices/show_pat_info.php";
    Intent intent;
    JSONObject jObject;
    Bundle bundle1;
    Pat_data pat_data;
    String pat_name, pat_id , doc_id, caregiver_name,contact_no, gender, dob, phy_name, phy_contact;

    public PatientInfo() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_patient_info, container, false);

        builder = new AlertDialog.Builder(getContext());
        dialog = new ProgressDialog(getContext());
        bundle1 = new Bundle();

       // textView = (TextView)v.findViewById(R.id.nm);
        Bundle bundle = getActivity().getIntent().getExtras();
        final String show_id = bundle.getString("doc_id");
        listView = (ListView)v.findViewById(R.id.child_info);
        list = new ArrayList<>();
        list1 = new ArrayList<>();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, base_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try{

                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    for(int x =0 ; x<jsonArray.length(); x++){
                        jObject = jsonArray.getJSONObject(x);

                        pat_name = jObject.getString("patient_name");
                        pat_id = jObject.getString("patient_id");
                        doc_id= jObject.getString("doc_id");
                        caregiver_name = jObject.getString("name_of_caregiver");
                        dob = jObject.getString("dob");
                        gender = jObject.getString("gender");
                        phy_name = jObject.getString("physician_name");
                        phy_contact = jObject.getString("physician_contact");
                        contact_no = jObject.getString("contact_no");

                        pat_data = new Pat_data(doc_id,pat_id,pat_name,caregiver_name,dob,contact_no,gender,phy_name,phy_contact);
                        list.add(pat_data);

                        list1.add(pat_name);
                        listView.setAdapter(new ArrayAdapter<>(getContext(),android.R.layout.simple_list_item_1, list1));

                    }


//                    bundle1.putString("doc_id", doc_id);
//                    bundle1.putString("name_of_cg",caregiver_name);
//                    bundle1.putString("contact_no", contact_no);
//                    bundle1.putString("dob", dob);
//                    bundle1.putString("pat_gender", gender);
//                    bundle1.putString("phy_name", phy_name);
//                    bundle1.putString("phy_contact", phy_contact);
//

//
                      startActivity(intent);

                    Log.d("Bundle", intent.toString());

                }catch(Exception e){
                    e.printStackTrace();
                    Log.e("Stack" , e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Toast.makeText(getContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();

                map.put("doc_id",show_id);

                return map;
            }
        };

        MySingleton.getInstance(getContext()).addToRequestQueue(stringRequest);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                pat_data = list.get(position);

                intent = new Intent(getContext(), Patient_Profile.class);

                bundle1.putString("pat_id",pat_data.getPat_id());
                bundle1.putString("patient_name",pat_data.getPat_name());
                bundle1.putString("doc_id",pat_data.getDoc_id());
                bundle1.putString("caregiver",pat_data.getCaregiver());
                bundle1.putString("dob",pat_data.getDob());
                bundle1.putString("contact_no",pat_data.getContact_no());
                bundle1.putString("gender",pat_data.getGender());
                bundle1.putString("phy_name",pat_data.getPhy_name());
                bundle1.putString("phy_contact",pat_data.getPhy_contact());

                intent.putExtras(bundle1);
                startActivity(intent);
            }
        });

        return v;
    }
}
