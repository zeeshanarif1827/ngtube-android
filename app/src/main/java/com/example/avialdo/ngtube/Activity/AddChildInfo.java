package com.example.avialdo.ngtube.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.avialdo.ngtube.MySingleton;
import com.example.avialdo.ngtube.R;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.HashMap;
import java.util.Map;

public class AddChildInfo extends AppCompatActivity {

    MaterialBetterSpinner betterSpinner;
    Button done;
    String base_url = "https://wwwubit.000webhostapp.com/NgServices/update_pat_data.php";
    AlertDialog.Builder builder;
    EditText child_id, child_caregiver, child_contact,child_dob,child_phy_name,child_phy_contact;
    String et_child_id, et_child_care, et_child_contact, et_child_dob, et_child_phyname, et_child_phycontact, et_child_gender;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_child_info);

        final String[] GENDER = {"select", "Male","FeMale"};
        betterSpinner = (MaterialBetterSpinner)findViewById(R.id.child_gender);
        ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line, GENDER);
        betterSpinner.setAdapter(genderAdapter);

        child_id = (EditText) findViewById(R.id.child_id);
        child_caregiver = (EditText) findViewById(R.id.child_caregiver);
        child_contact = (EditText) findViewById(R.id.child_contact);
        child_dob = (EditText) findViewById(R.id.child_dob);
        child_phy_name = (EditText) findViewById(R.id.child_phy_name);
        child_phy_contact = (EditText) findViewById(R.id.child_phy_contact);

        done = (Button)findViewById(R.id.submit_child_info);

        builder = new AlertDialog.Builder(this);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                et_child_id = child_id.getText().toString();
                et_child_care = child_caregiver.getText().toString();
                et_child_dob = child_dob.getText().toString();
                et_child_contact = child_contact.getText().toString();
                et_child_phyname = child_phy_name.getText().toString();
                et_child_phycontact = child_phy_contact.getText().toString();
                et_child_gender = betterSpinner.getText().toString();

                if (et_child_id.equals("") || et_child_care.equals("") || et_child_dob.equals("") || et_child_contact.equals("")
                        || et_child_phyname.equals("") || et_child_phycontact.equals("")) {

                    builder.setTitle("Server Response");
                    builder.setMessage("Please fill the Remaining Fields...");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            child_id.setText("");
                            child_caregiver.setText("");
                            child_dob.setText("");
                            child_contact.setText("");
                            child_phy_name.setText("");
                            child_phy_contact.setText("");
                            betterSpinner.setText("");

                        }
                    });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
                else{

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, base_url, new Response.Listener<String>() {

                        public void onResponse(String response) {

                            builder.setTitle("Server Response");
                            builder.setMessage("Response" + response);
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    child_id.setText("");
                                    child_caregiver.setText("");
                                    child_dob.setText("");
                                    child_contact.setText("");
                                    child_phy_name.setText("");
                                    child_phy_contact.setText("");
                                    betterSpinner.setText("");
                                }
                            });
                            AlertDialog alertDialog = builder.create();
                            alertDialog.show();
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            error.printStackTrace();
                            Log.d("Stack", error.toString());
                        }

                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();

                            params.put("patient_id", et_child_id);
                            params.put("name_of_caregiver", et_child_care);
                            params.put("contact_no", et_child_contact);
                            params.put("dob", et_child_dob);
                            params.put("gender", et_child_gender);
                            params.put("physician_name", et_child_phyname);
                            params.put("physician_contact", et_child_phycontact);

                            return params;
                        }
                    };

                    MySingleton.getInstance(AddChildInfo.this).addToRequestQueue(stringRequest);

                }
            }
        });
    }
}
