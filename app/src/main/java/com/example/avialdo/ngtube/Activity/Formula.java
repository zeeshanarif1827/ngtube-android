package com.example.avialdo.ngtube.Activity;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.avialdo.ngtube.R;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

public class Formula extends AppCompatActivity {

    MaterialBetterSpinner patient_name , entry;
    TextInputLayout layout_child_name;
    Button manually , date;
    LinearLayout food;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formula);

        entry = (MaterialBetterSpinner)findViewById(R.id.formula_entry) ;

        layout_child_name = (TextInputLayout)findViewById(R.id.layout_child_name);
        manually = (Button)findViewById(R.id.manually_formula);
        date = (Button)findViewById(R.id.datepick);

        food = (LinearLayout)findViewById(R.id.food);

        final String[]FORMULAS = {"ABC", "XYZ", "DEF", "JKL"};
        ArrayAdapter<String> entryAdapter = new ArrayAdapter<String>(Formula.this,android.R.layout.simple_dropdown_item_1line, FORMULAS);
        entry.setAdapter(entryAdapter);

        manually.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              food.setVisibility(View.VISIBLE);
            }
        });
    }
}
