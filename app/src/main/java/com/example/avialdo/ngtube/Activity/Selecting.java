package com.example.avialdo.ngtube.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.avialdo.ngtube.R;

public class Selecting extends AppCompatActivity {

    Button select_login , select_sign_up;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selecting);

        select_login = (Button)findViewById(R.id.select_login);
        select_sign_up = (Button)findViewById(R.id.select_signup);

        select_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Selecting.this, Login.class));
            }
        });

        select_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Selecting.this,SignUp.class));
            }
        });

    }
}
