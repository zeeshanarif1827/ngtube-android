package com.example.avialdo.ngtube.Fragments;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.avialdo.ngtube.MySingleton;
import com.example.avialdo.ngtube.R;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class Update_Doc_Data extends Fragment {

    EditText update_name , update_cell , update_pass, conf_pass;
    MaterialBetterSpinner update_city, update_speciality;
    AlertDialog.Builder builder;
    String base_url = "https://wwwubit.000webhostapp.com/NgServices/update_data.php";
    Button button;
    ProgressDialog dialog;
    String up_name ,up_cell, up_pass , up_conf , up_city, up_spec;

    public Update_Doc_Data() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_update__doc__data, container, false);

        builder = new AlertDialog.Builder(getContext());
        dialog = new ProgressDialog(getContext());
        final AlertDialog alertDialog = builder.create();

        button = (Button)v.findViewById(R.id.update_data);

        update_name = (EditText)v.findViewById(R.id.update_name);
        update_pass = (EditText)v.findViewById(R.id.update_pass);
        update_cell = (EditText)v.findViewById(R.id.update_cell);
        conf_pass = (EditText)v.findViewById(R.id.conf_update_pass);


        update_city = (MaterialBetterSpinner)v.findViewById(R.id.update_city);
        update_speciality = (MaterialBetterSpinner)v.findViewById(R.id.update_speciality);

        final String[] SPECIALIST = {"select", "Cardiology_Surgeon", "Dermotology_Surgeon", "Orthopedic_Surgeon", "Neuro_Surgeon" , "Other"};
        final String[] CITY = {"select", "Karachi","Lahore","Multan","Islamabad"};

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getContext(),android.R.layout.simple_dropdown_item_1line, SPECIALIST);
        ArrayAdapter<String> cityAdapter = new ArrayAdapter<String>(getContext(),android.R.layout.simple_dropdown_item_1line, CITY);

        update_speciality.setAdapter(arrayAdapter);
        update_city.setAdapter(cityAdapter);

        Bundle bundle = getActivity().getIntent().getExtras();
        final String show_email = bundle.getString("doc_email");

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                up_name = update_name.getText().toString();
                up_cell = update_cell.getText().toString();
                up_pass = update_pass.getText().toString();
                up_conf = conf_pass.getText().toString();
                up_city = update_city.getText().toString();
                up_spec = update_speciality.getText().toString();


                if(!up_pass.equals(up_conf)){
                    builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Server Response");
                    builder.setMessage("Password And Confirm Password Does Not Match..");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            update_name.setText("");
                            update_city.setText("");
                            update_cell.setText("");
                            update_speciality.setText("");
                            conf_pass.setText("");
                            update_pass.setText("");

                        }
                    });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }

                if (up_name.equals("") || up_pass.equals("") || up_conf.equals("") || up_spec.equals("") || up_city.equals("") || up_cell.equals("")) {

                    builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Server Response");
                    builder.setMessage("Please fill the Remaining Fields...");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            update_name.setText("");
                            update_city.setText("");
                            update_cell.setText("");
                            update_speciality.setText("");
                            conf_pass.setText("");
                            update_pass.setText("");


                        }
                    });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }

                else{
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, base_url, new Response.Listener<String>() {

                        public void onResponse(String response) {
                            builder = new AlertDialog.Builder(getContext());
                            builder.setTitle("Server Response");
                            builder.setMessage("Response" + response);
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    update_name.setText("");
                                    update_city.setText("");
                                    update_cell.setText("");
                                    update_speciality.setText("");
                                    conf_pass.setText("");
                                    update_pass.setText("");

                                }
                            });
                            AlertDialog alertDialog = builder.create();
                            alertDialog.show();
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            error.printStackTrace();
                            Log.d("Stack", error.toString());
                        }

                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();

                            params.put("doc_name", up_name);
                            params.put("doc_email", show_email);
                            params.put("doc_password", up_pass);
                            params.put("doc_city", up_city);
                            params.put("doc_cell", up_cell);
                            params.put("doc_speciality", up_spec);

                            return params;
                        }
                    };

                    MySingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
                }
            }
        });

        return v;
    }

}
