package com.example.avialdo.ngtube.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.avialdo.ngtube.Fragments.AddReminder;
import com.example.avialdo.ngtube.Fragments.Diary;
import com.example.avialdo.ngtube.Fragments.Main;
import com.example.avialdo.ngtube.Fragments.MyProfile;
import com.example.avialdo.ngtube.Fragments.Nutrition;
import com.example.avialdo.ngtube.Fragments.PatientInfo;
import com.example.avialdo.ngtube.Fragments.ViewReminder;
import com.example.avialdo.ngtube.R;

public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    FragmentTransaction fragmentTransaction;
    TextView header_name, header_email;

    @Nullable

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.content_home, new Main());
        fragmentTransaction.commit();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        header_email = (TextView) navigationView.getHeaderView(0).findViewById(R.id.header_email);
        header_name = (TextView) navigationView.getHeaderView(0).findViewById(R.id.header_name);

        Bundle bundle = getIntent().getExtras();

        header_name.setText(bundle.getString("doc_name"));
        header_email.setText(bundle.getString("doc_email"));

    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);

        } else {
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.content_home, new Main());
            fragmentTransaction.commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(Home.this, Login.class));
        }

        if (id == R.id.add_child) {
            startActivity(new Intent(Home.this, AddChildInfo.class));
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        switch (item.getItemId()) {

            case R.id.home:
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.content_home, new Main());
                fragmentTransaction.commit();

                item.setChecked(true);
                getSupportActionBar().setTitle("NG Tube");
                break;

            case R.id.diary:
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.content_home, new Diary());
                fragmentTransaction.commit();

                item.setChecked(true);
                getSupportActionBar().setTitle("Diary");
                break;

            case R.id.nutrition:
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.content_home, new Nutrition());
                fragmentTransaction.commit();

                item.setChecked(true);
                getSupportActionBar().setTitle("Nutrition Goal");
                break;

            case R.id.info:
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.content_home, new PatientInfo());
                fragmentTransaction.commit();

                item.setChecked(true);
                getSupportActionBar().setTitle("Patient Information");
                break;

            case R.id.add_reminder:
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.content_home, new AddReminder());
                fragmentTransaction.commit();

                item.setChecked(true);
                getSupportActionBar().setTitle("Add Reminder");
                break;

            case R.id.view_reminder:
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.content_home, new ViewReminder());
                fragmentTransaction.commit();

                item.setChecked(true);
                getSupportActionBar().setTitle("View Reminder");
                break;

            case R.id.profile:
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.content_home, new MyProfile());
                fragmentTransaction.commit();

                item.setChecked(true);
                getSupportActionBar().setTitle("My Profile");
                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
