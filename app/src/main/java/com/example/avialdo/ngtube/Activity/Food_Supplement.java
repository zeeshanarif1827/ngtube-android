package com.example.avialdo.ngtube.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.avialdo.ngtube.R;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

public class Food_Supplement extends AppCompatActivity {

    Button insert_food;
    LinearLayout linearLayout;
    MaterialBetterSpinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food__supplement);

        insert_food = (Button)findViewById(R.id.insert_food);
        linearLayout = (LinearLayout)findViewById(R.id.food);

        spinner = (MaterialBetterSpinner)findViewById(R.id.show_food);

        String[] FOODS = {"Apple", "Banana", "Orange"};
        ArrayAdapter<String> foodAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line, FOODS);

        spinner.setAdapter(foodAdapter);

        insert_food.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayout.setVisibility(View.VISIBLE);
            }
        });
    }
}
