package com.example.avialdo.ngtube.Fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.avialdo.ngtube.MySingleton;
import com.example.avialdo.ngtube.R;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class Main extends Fragment {

    TextInputLayout layout_pat_name;
    EditText child_name;
    String pat_name;
    Button button;
    Intent intent;
    String base_url = "https://wwwubit.000webhostapp.com/NgServices/patient_info.php";
    AlertDialog.Builder builder;

    public Main() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_main, container, false);

        layout_pat_name = (TextInputLayout)v.findViewById(R.id.layout_pat_name);
        child_name = (EditText)v.findViewById(R.id.pat_name);
        button = (Button)v.findViewById(R.id.enter_pat_name);

        builder = new AlertDialog.Builder(getContext());
        AlertDialog alertDialog = builder.create();

        Bundle bundle = getActivity().getIntent().getExtras();
        final String show = bundle.getString("doc_id");

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pat_name = child_name.getText().toString();

                StringRequest stringRequest = new StringRequest(Request.Method.POST, base_url, new Response.Listener<String>() {

                    public void onResponse(String response) {
                        builder = new AlertDialog.Builder(getContext());
                        builder.setTitle("Server Response");
                        builder.setMessage("Response" + response);
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                               child_name.setText("");
                            }
                        });
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        error.printStackTrace();
                        Log.d("Stack", error.toString());
                    }

                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> params = new HashMap<String, String>();

                        params.put("doc_id", show);
                        params.put("patient_name", pat_name);

                        return params;
                    }
                };

                MySingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
            }
        });


        return v;
    }

}
