package com.example.avialdo.ngtube.Fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.avialdo.ngtube.MySingleton;
import com.example.avialdo.ngtube.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class Nutrition extends Fragment {

    EditText insert_id, insert_fiber, insert_protien, insert_water, insert_calories,insert_volume, insert_perfeeding;
    DatePicker insert_date;
    Button insert_done;
    StringBuilder stringBuilder;
    String base_url = "https://wwwubit.000webhostapp.com/NgServices/nutrition_goals.php";
    AlertDialog.Builder builder;
    String et_doc_id, et_insert_id, et_insert_fiber, et_insert_calories, et_insert_protien,
            et_insert_water, et_insert_volume;

    int day,month, year;

    public Nutrition() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(false);
        View view = inflater.inflate(R.layout.fragment_nutrition, container, false);



        builder = new AlertDialog.Builder(getContext());
        stringBuilder = new StringBuilder();

        insert_id = (EditText) view.findViewById(R.id.insert_id);
        insert_date = (DatePicker) view.findViewById(R.id.insert_date);
        insert_fiber = (EditText) view.findViewById(R.id.insert_fiber);
        insert_protien = (EditText) view.findViewById(R.id.insert_protien);
        insert_water = (EditText) view.findViewById(R.id.insert_water);
        insert_calories = (EditText) view.findViewById(R.id.insert_calories);
        insert_volume = (EditText) view.findViewById(R.id.insert_volume);

        insert_done = (Button) view.findViewById(R.id.insert_done);


        Bundle bundle = getActivity().getIntent().getExtras();
        et_doc_id = bundle.getString("doc_id");

        insert_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                et_insert_id = insert_id.getText().toString();
                et_insert_volume = insert_volume.getText().toString();
                et_insert_protien = insert_protien.getText().toString();
                et_insert_fiber = insert_fiber.getText().toString();
                et_insert_calories = insert_calories.getText().toString();
                et_insert_water = insert_water.getText().toString();

                day = insert_date.getDayOfMonth();
                month = insert_date.getMonth()+1;
                year = insert_date.getYear();

                final String date = String.valueOf(day +"-"+ month+"-"+year);

                if (et_insert_id.equals("") || et_insert_water.equals("") || et_insert_calories.equals("") ||
                        et_insert_fiber.equals("") ||et_insert_protien.equals("") || et_insert_volume.equals("")) {

                    builder.setTitle("Something Went Wrong");
                    builder.setMessage("Please fill all the fields");
                    displayAlert("input error");
                }

                else{
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, base_url, new Response.Listener<String>() {

                        public void onResponse(String response) {
                            try {

                                JSONArray jsonArray = new JSONArray(response);
                                Log.e("Error", response.toString());
                                JSONObject jsonObject = jsonArray.getJSONObject(0);
                                String code = jsonObject.getString("code");
                                String message = jsonObject.getString("message");

                                builder.setTitle("Server Response");
                                builder.setMessage(message);
                                displayAlert(code);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            error.printStackTrace();
                            Log.d("Stack", error.toString());
                        }

                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();

                            params.put("doc_id", et_doc_id);
                            params.put("patient_id", et_insert_id);
                            params.put("feed_date", date);
                            params.put("volume", et_insert_volume);
                            params.put("calories", et_insert_calories);
                            params.put("protien", et_insert_protien);
                            params.put("fiber", et_insert_fiber);
                            params.put("water", et_insert_water);

                            return params;
                        }
                    };

                    MySingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
                }
            }
        });

        return view;
    }

    private void displayAlert(final String code) {

       builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
           @Override
           public void onClick(DialogInterface dialog, int which) {

               if(code.equals("input error")){
                   insert_id.setText("");
                   insert_volume.setText("");
                   insert_calories.setText("");
                   insert_water.setText("");
                   insert_protien.setText("");
                   insert_fiber.setText("");
               }
               else if(code.equals("registration success")){
                   getActivity().finish();
               } else if (code.equals("registration_failed")) {

                   insert_id.setText("");
                   insert_volume.setText("");
                   insert_calories.setText("");
                   insert_water.setText("");
                   insert_protien.setText("");
                   insert_fiber.setText("");

               }
           }
       });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }



}
