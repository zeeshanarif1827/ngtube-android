package com.example.avialdo.ngtube.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.avialdo.ngtube.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class ViewReminder extends Fragment {


    public ViewReminder() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(false);
        return inflater.inflate(R.layout.reminder_adapter, container, false);
    }

}
