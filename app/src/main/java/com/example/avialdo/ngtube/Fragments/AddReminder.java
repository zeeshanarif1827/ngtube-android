package com.example.avialdo.ngtube.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.avialdo.ngtube.R;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddReminder extends Fragment {

    MaterialBetterSpinner reminder_f , reminder_t;
    public AddReminder() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(false);
        View v = inflater.inflate(R.layout.fragment_add_reminder, container, false);

        reminder_t = (MaterialBetterSpinner)v.findViewById(R.id.reminder_t);
        reminder_f = (MaterialBetterSpinner)v.findViewById(R.id.reminder_f);


        String[] reminder_type = {"Formula"};
        String[] reminder_for = {"Saad" , "Asad" , "Ali"};

        final ArrayAdapter<String> arrayAdapter1 = new ArrayAdapter<String>(getContext(),android.R.layout.simple_dropdown_item_1line, reminder_type);
        reminder_t.setAdapter(arrayAdapter1);
        final ArrayAdapter<String> arrayAdapter2 = new ArrayAdapter<String>(getContext(),android.R.layout.simple_dropdown_item_1line, reminder_for);
        reminder_f.setAdapter(arrayAdapter2);

        return v;
    }

}
